# Erik Kwiatkowski Lab Notebook

## 2/6/2024: First meeting with TA
Met with our TA to speak about the project and upcoming deadlines. Showed all of the pieces of project that were required for meeting. High level requirements need to fixed up, block diagram looks good, as well as the rest of project proposal.

## 2/8/2024: Team Contract and Project proposal
Completed work on project proposal and turned it in. Worked on team contract, wrote out group roles and project goals.

## 2/9/2024: Team Contract
Completed team contract and turned it in. Made plan of future work on the project.

## 2/13/2024: Meeting with Tianxiang
Spoke about upcoming design review and what should be ready before the deadline. Need to change the project proposal format of subsystem to requirements and verifications table to show how we will prove the subsystems work. 

## 2/21/2024: Group Meeting
We met up together to discuss our design document and some of the specifics of our project. Decided on specific parts that would be used for the project as well as creating a schedule of what should be done and when.

## 2/22/2024: Work on Design Document
Completed work on design document to be able to turn it in. Made small changes to design.

## 3/5/2024: Meeting with Tianxiang
Met with Tianxiang before break to discuss progress and what should be done over break. Need to progress on PCB. We ordered many pieces before this meeting and mentioned that pieces will be coming in soon to begin testing.

## 3/19/2024: Meeting with Tianxiang
Met with TA after spring break. We talked about progress that has been made in code, as well as progress with the physical design at the machine shop. 

## 3/27/2024: Updated Physical Design
Worked on Physical design to update it with new positioning of motors, as well as adding the LEDs.

## 4/2/2024: Meeting with Tianxiang
Met with Tianxiang to discuss progress on project. Did not have PCB completed so could not order next PCB. Need to complete PCB as soon as possible as it is very important.

## 4/9/2024: Ordered Next PCB
Did not have meeting with Tianxiang, instead worked on PCB for final order deadline, needed to fix later at night after urgent email to add edge cuts layer, as well as send gerber files.

## 4/16/2024: Mock Demo
Met with Tianxiang to demo current iteration of project. We used arduino temporarily to show the progress we have. Told to move to breadboard if we cannot figure out PCB.

## 4/19/2024: Worked on Contract Fulfillment
Worked on contract fulfillment document. Mentioned how project was going as well as wrote about differences between reality and contract at beginning.

## 4/21/2024: Group Meeting
Met as a group to work on the project and see how our project will look at final demo. Will try PCB but do not know if will get it working on time. Have breadboard iteration of project ready and working.

## 4/21/2024: Work on PCB
Met with Ginny at ECEB to work on PCB soldering. Completed soldering and tried to see if we could get it to run.

## 4/23/2024: Final Demo
Went into final demo with breadboard iteration, could not get PCB to work correctly with project.

## 4/25/2024: Mock Presentation
Completed mock presentation and began working on fixing up presentation as directed by Communications TA, as well as other 2 ECE TAs. 

## 4/28/2024: Work on Final Presentation
Worked to fix the presentation for final presentation on 4/29. Practiced what to say during presentation.